package com.personal.restaurantorderapp;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.cardview.widget.CardView;
import androidx.fragment.app.FragmentManager;

import com.bumptech.glide.Glide;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;

public class FavoritesAdapter extends BaseAdapter {
    private ArrayList<OrderedItem> products;
    private Context context;
    private LocalDatabase db;
    private LayoutInflater inflater;
    private FirebaseFirestore firestore;
    private String category;
    private FragmentManager fragmentManager;
 public FavoritesAdapter(ArrayList<OrderedItem> products,Context context,LayoutInflater inflater,FragmentManager fragmentManager) {
     this.inflater=inflater;
     this.products=products;
     this.context=context;
     this.fragmentManager=fragmentManager;
 }
    @Override
    public int getCount() {
        return products.size();
    }

    @Override
    public Object getItem(int position) {
        return products.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
     if(convertView==null)
         convertView= LayoutInflater.from(context).inflate(R.layout.favorite_item,parent,false);
         final ImageView productIcon=convertView.findViewById(R.id.productIcon);
         final ImageView favoriteIcon=convertView.findViewById(R.id.imageview_favorite);
         TextView productName=convertView.findViewById(R.id.productName);
         TextView productPrice=convertView.findViewById(R.id.productPrice);
         Button addBttn=convertView.findViewById(R.id.addButton);
         Button removeButton = convertView.findViewById(R.id.removeButton);
         Glide.with(context).load(products.get(position).getIcon()).centerCrop().into(productIcon);
         productName.setText(products.get(position).getName());
         productPrice.setText(products.get(position).getPrice()+ " lei");


         productIcon.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View v) {
                 ProductDialog exampleDialog = new ProductDialog(products.get(position).getIcon(),products.get(position).getDescription(),context);
                 exampleDialog.show(fragmentManager, "example dialog");
             }
         });

         db=new LocalDatabase(context);

         addBttn.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View v) {
                 addToast();
                 db.addToCart(products.get(position));
             }
         });

         removeButton.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View v) {

                 LocalDatabase db = new LocalDatabase(context);
                 db.removeFavorite(products.get(position).getId());
                 favoriteToast("Produsul a fost sters de la favorite !");
                 products.get(position).setIsFavorite("false");
                 firestore=FirebaseFirestore.getInstance();

                 switch(products.get(position).getId()/100) {
                     case 1:
                         category="Deserturi";
                     break;
                     case 2:
                         category="Felul 1";
                         break;
                     case 3:
                         category="Sosuri";
                         break;
                     case 4:
                         category="Fripturi";
                         break;
                     case 5:
                         category="Salata";

                 }
                 Log.d("categorie",category);
                 firestore.collection(category).document(products.get(position).getName()).set(products.get(position));
                 products.remove(position);
                 notifyDataSetChanged();

             }
         });


        return convertView;
    }

    private void addToast() {
        View layout = inflater.inflate(R.layout.adding_toast ,null);
        Toast toast = new Toast(context);
        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 700);
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.setView(layout);
        toast.show();
    }

    private void favoriteToast(String text) {
        View layout = inflater.inflate(R.layout.adding_toast,null);
        TextView toastTV=layout.findViewById(R.id.toastText);
        ImageView toastIV=layout.findViewById(R.id.toastIcon);
        CardView toastCV=layout.findViewById(R.id.toastCV);
        toastTV.setText(text);
        Glide.with(context).load(R.drawable.deleted_icon).into(toastIV);
        toastCV.setCardBackgroundColor(Color.parseColor("#FF4081"));
        Toast toast = new Toast(context);
        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 700);
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.setView(layout);
        toast.show();
    }


}
