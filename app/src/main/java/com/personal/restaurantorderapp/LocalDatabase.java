package com.personal.restaurantorderapp;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.view.SubMenu;

import androidx.annotation.Nullable;

import com.google.firestore.v1.StructuredQuery;
import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

import java.util.ArrayList;

public class LocalDatabase extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME="FoodDatabase";
    private static final String TABLE_PRODUCTS="Products";
    private static final String TABLE_CART="ShoppingCart";
    private static final String TABLE_Favorites="Favorites";

    private static final String KEY_ID="id";
    private static final String KEY_NAME="name";
    private static final String KEY_PRICE="price";
    private static final String KEY_ICON= "icon";
    private static final String KEY_FAVORITE="favorite";
    private static final String KEY_CART_QUANTITY="quantity";

    private static final String CREATE_TABLE_PRODUCTS="CREATE TABLE "+TABLE_PRODUCTS+"("+
            KEY_ID+ " INTEGER PRIMARY KEY,"+
            KEY_NAME+" TEXT,"+
            KEY_PRICE+" INTEGER,"+
            KEY_ICON+" TEXT,"+
            KEY_FAVORITE+" TEXT )";

    private static final String CREATE_TABLE_CART="CREATE TABLE "+TABLE_CART+"("+
            KEY_ID+" INTEGER ,"+
            KEY_NAME+" TEXT,"+
            KEY_PRICE+" INTEGER,"+
            KEY_ICON+" TEXT,"+
            KEY_FAVORITE+" TEXT,"+
            KEY_CART_QUANTITY+ " INTEGER)";

    public static final String CREATE_TABLE_Favorites="CREATE TABLE "+ TABLE_Favorites+"("+
            KEY_ID+" INTEGER PRIMARY KEY,"+
            KEY_NAME+" TEXT,"+
            KEY_PRICE+" INTEGER,"+
            KEY_ICON+" TEXT)";


    public LocalDatabase(@Nullable Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }



    @Override
    public void onCreate(SQLiteDatabase db) {
       db.execSQL(CREATE_TABLE_PRODUCTS);
       db.execSQL(CREATE_TABLE_CART);
       db.execSQL(CREATE_TABLE_Favorites);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PRODUCTS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CART);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_Favorites);

        onCreate(db);
    }

    public void  addProduct(OrderItem item) {

        SQLiteDatabase db=this.getWritableDatabase();
        ContentValues contentValues=new ContentValues();
        contentValues.put(KEY_ID,item.getId());
        contentValues.put(KEY_NAME,item.getName());
        contentValues.put(KEY_PRICE,item.getPrice());
        contentValues.put(KEY_ICON,item.getPrice());



    }

    public ArrayList<OrderedItem> getCart() {
      ArrayList<OrderedItem> items=new ArrayList<OrderedItem>();
      String selectQuery="SELECT* FROM "+TABLE_CART;

      SQLiteDatabase db=this.getReadableDatabase();
      Cursor c=db.rawQuery(selectQuery,null);

      if(c.moveToFirst()) {
          do {
              OrderedItem i=new OrderedItem();
              i.setId(c.getInt(c.getColumnIndex(KEY_ID)));
              i.setName(c.getString(c.getColumnIndex(KEY_NAME)));
              i.setIcon(c.getString(c.getColumnIndex(KEY_ICON)));
              i.setPrice(c.getInt(c.getColumnIndex(KEY_PRICE)));
              i.setQuantity(c.getInt(c.getColumnIndex(KEY_CART_QUANTITY)));
              i.setIsFavorite(c.getString(c.getColumnIndex(KEY_FAVORITE)));

              items.add(i);
          } while(c.moveToNext());
      }
      return items;

    }

    public void deleteCartItem(OrderedItem item) {
        SQLiteDatabase db=this.getWritableDatabase();
        db.delete(TABLE_CART,KEY_NAME+ " = ?",new String[] {item.getName()});
    }

    public void clearCart() {
        SQLiteDatabase db=this.getWritableDatabase();
        db.delete(TABLE_CART,null,null);
    }

    public void addToCart(OrderItem item) {
        SQLiteDatabase db=this.getWritableDatabase();
        String query="SELECT* FROM "+ TABLE_CART + " WHERE " + KEY_ID + " = " + item.getId();
        Cursor c=db.rawQuery(query,null);
        int i=c.getCount();
        if(i==0) {
            ContentValues contentValues = new ContentValues();
            contentValues.put(KEY_ID, item.getId());
            contentValues.put(KEY_NAME, item.getName());
            contentValues.put(KEY_PRICE, item.getPrice());
            contentValues.put(KEY_ICON, item.getIcon());
            contentValues.put(KEY_CART_QUANTITY, 1);
            contentValues.put(KEY_FAVORITE, "false");
            db.insert(TABLE_CART, null, contentValues);
        }
        else
        addQuantity(item.getId());
    }

    public void addQuantity(int id) {
        SQLiteDatabase db=this.getWritableDatabase();
        String incrementQuery="UPDATE "+ TABLE_CART + " SET " + KEY_CART_QUANTITY + " = " + KEY_CART_QUANTITY + "+1 WHERE " + KEY_ID + " = " + id;
        db.execSQL(incrementQuery);
    }

    public void removeQuantity(int id) {
        SQLiteDatabase db=this.getWritableDatabase();
        String incrementQuery="UPDATE "+ TABLE_CART + " SET " + KEY_CART_QUANTITY + " = " + KEY_CART_QUANTITY + "-1 WHERE " + KEY_ID + " = " +id;
        db.execSQL(incrementQuery);
    }
    public void addToFavorites(int id) {
        SQLiteDatabase db=this.getWritableDatabase();
        String query="UPDATE "+ TABLE_CART + " SET " + KEY_FAVORITE + " = " + " 'true' WHERE "+ KEY_ID + " = " + id;
        db.execSQL(query);
    }

    public void removeFavorite(int id) {
        SQLiteDatabase db=this.getWritableDatabase();
        String query="UPDATE "+ TABLE_CART + " SET " + KEY_FAVORITE + " = " + " 'false' WHERE "+ KEY_ID + " = " + id;
        db.execSQL(query);
    }

    public ArrayList<OrderedItem> getFavorites() {
        ArrayList<OrderedItem> items = new ArrayList<>();
        SQLiteDatabase db=this.getReadableDatabase();
        String query="SELECT* FROM "+TABLE_CART + " WHERE " + KEY_FAVORITE + "=" + "true";

        Cursor c = db.rawQuery(query,null);
        if(c.moveToFirst())
            do {
                OrderedItem i=new OrderedItem();
                i.setId(c.getInt(c.getColumnIndex(KEY_ID)));
                i.setName(c.getString(c.getColumnIndex(KEY_NAME)));
                i.setIcon(c.getString(c.getColumnIndex(KEY_ICON)));
                i.setPrice(c.getInt(c.getColumnIndex(KEY_PRICE)));
                i.setQuantity(c.getInt(c.getColumnIndex(KEY_CART_QUANTITY)));
                i.setIsFavorite(c.getString(c.getColumnIndex(KEY_FAVORITE)));

                items.add(i);

            } while(c.moveToNext());

        return  items;
    }


}

