package com.personal.restaurantorderapp;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.FirebaseFirestore;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

public class OrderActivity extends AppCompatActivity {
private RecyclerView orderRV;
private OrderAdapter adapter;
private TextView orderNumber;
private Button send;
private TextView totalTV;
private Button cancel;
private ArrayList<OrderedItem> items;
private TextView table;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order);

        orderRV=findViewById(R.id.orderRV);
        orderNumber=findViewById(R.id.orderNumber);
        send=findViewById(R.id.sendButton);
        totalTV=findViewById(R.id.total);
        cancel=findViewById(R.id.cancelButton);
        table=findViewById(R.id.table);
        items=new LocalDatabase(getApplicationContext()).getCart();

        adapter=new OrderAdapter(items,getApplicationContext());
        RecyclerView.LayoutManager layoutManager=new LinearLayoutManager(this);
        orderRV.setLayoutManager(layoutManager);
        orderRV.setAdapter(adapter);

        SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy_mmHH", Locale.getDefault());
        String currentDateandTime = sdf.format(new Date());
        orderNumber.setText("Comanda #"+currentDateandTime);
        totalTV.setText("Total: "+ getIntent().getIntExtra("total",0)+" lei");
        table.setText("Masa: #"+getIntent().getStringExtra("table"));
        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendOrder();
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(),MainActivity.class));
            }
        });


    }

    private void sendOrder() {
        if(items.isEmpty())
            errorToast();
        else {
            FirebaseFirestore db=FirebaseFirestore.getInstance();
            final HashMap<String, Object> order = new HashMap<>();
            SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy_mmHH", Locale.getDefault());
            String currentDateandTime = sdf.format(new Date());
            order.put("date",currentDateandTime);

            for(int i=0;i<items.size();i++) {
                order.put("product_name" + i, items.get(i).getName());
                order.put("qty_product" + i, items.get(i).getQuantity());
                order.put("price_product"+i,items.get(i).getPrice());
                order.put("table_number",getIntent().getStringExtra("table"));
                order.put("preferences",getIntent().getStringExtra("preferences"));
                order.put("total",getIntent().getIntExtra("total",0));


            }
            db.collection("orders").document().set(order).addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {
                    orderToast("Comanda a fost trimisa cu succes!");
                }
            });

            new LocalDatabase(getApplicationContext()).clearCart();
            new CountDownTimer(1000,50) {

                @Override
                public void onTick(long millisUntilFinished) {
                    long finishedSeconds = 1000 - millisUntilFinished;
                    int total = (int) (((float)finishedSeconds / (float)1000) * 100.0);


                }

                @Override
                public void onFinish() {
                    startActivity(new Intent(getApplicationContext(),CategoryActivity.class));
                    finish();
                }
            }.start();

        }
    }

    private void errorToast() {

        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.adding_toast,null);
        TextView toastTV=layout.findViewById(R.id.toastText);
        ImageView toastIV=layout.findViewById(R.id.toastIcon);
        CardView toastCV=layout.findViewById(R.id.toastCV);
        toastTV.setText("Comanda trebuie sa contina cel putin un produs!");
        Glide.with(getApplicationContext()).load(R.drawable.deleted_icon).into(toastIV);
        toastCV.setCardBackgroundColor(Color.parseColor("#FF4081"));
        Toast toast = new Toast(getApplicationContext());
        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 700);
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.setView(layout);
        toast.show();
    }

    private void orderToast(String text) {
        LayoutInflater inflater=getLayoutInflater();
        View layout = inflater.inflate(R.layout.adding_toast,null);
        TextView toastTV=layout.findViewById(R.id.toastText);
        toastTV.setText(text);
        Toast toast = new Toast(getApplicationContext());
        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 700);
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.setView(layout);
        toast.show();
    }
    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
        startActivity(new Intent(getApplicationContext(), MainActivity.class));
        finish();

    }
}
