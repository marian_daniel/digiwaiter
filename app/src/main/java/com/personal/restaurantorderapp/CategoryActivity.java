package com.personal.restaurantorderapp;

import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.Locale;

import static android.view.Gravity.RIGHT;

public class CategoryActivity extends AppCompatActivity {
    private CategoryAdapter categoryAdapter;
    private RecyclerView.LayoutManager layoutManager;
    private RecyclerView categoryListRV;
    private ArrayList<Category> categories;
    private Intent intent;
   private Toolbar customToolbar;
   private ImageView menuButton;
   private ImageView shoppingCart;
    private DrawerLayout dl;
    private ActionBarDrawerToggle t;
    private NavigationView nv;
    private FirebaseFirestore firebaseDB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);
        customToolbar=findViewById(R.id.customToolbar);
        setSupportActionBar(customToolbar);

        Window window=this.getWindow();
        window.setStatusBarColor(getResources().getColor(R.color.colorRed));


        menuButton=customToolbar.findViewById(R.id.menuIcon);

        prepareNavigationDrawer();
        menuButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                dl.openDrawer(Gravity.START);
            }
        });

        shoppingCart=customToolbar.findViewById(R.id.shoppingCart);
        shoppingCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(getApplicationContext(),MainActivity.class);
                startActivity(intent);
            }
        });
        categories=new ArrayList<Category>();
        layoutManager=new LinearLayoutManager(this);
        categoryAdapter=new CategoryAdapter(categories,this);
        intent=new Intent(this,MealListActivity.class);
        categoryListRV=findViewById(R.id.categoryListRV);

        categoryListRV.setLayoutManager(layoutManager);
        prepareCategories();
        categoryListRV.setAdapter(categoryAdapter);
        categoryListRV.setHasFixedSize(true);



        categoryAdapter.setClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                int i=  categoryListRV.getChildLayoutPosition(v);
                intent.putExtra("name",categories.get(i).getName());
                startActivity(intent);

            }
        });

    }

    public void prepareCategories() {
        firebaseDB=FirebaseFirestore.getInstance();
        firebaseDB.collection("categories").get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                for(DocumentSnapshot querySnapshot: task.getResult()) {
                    Category category=new Category(querySnapshot.getString("icon"),querySnapshot.getString("name"));
                    categories.add(category);
                   categoryListRV.setAdapter(categoryAdapter);
                }
                }

        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

    return true;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id=item.getItemId();
        return super.onOptionsItemSelected(item);
    }

    public void  prepareNavigationDrawer() {
        dl = (DrawerLayout)findViewById(R.id.drawerLayout);
        t = new ActionBarDrawerToggle(this,dl,R.string.Open,R.string.Close);
        dl.addDrawerListener(t);
        t.syncState();

        nv=findViewById(R.id.nv);
        nv.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {

                switch(menuItem.getItemId()) {
                    case R.id.toFavorites:
                        Intent favoritesIntent=new Intent(getApplicationContext(),FavoritesActivity.class);
                        startActivity(favoritesIntent);
                        break;

                    case R.id.toCart:
                        Intent cartIntent=new Intent(getApplicationContext(),MainActivity.class);
                        startActivity(cartIntent);
                        break;

                    case R.id.toFeedback:
                        Intent feedBackIntent=new Intent(getApplicationContext(),FeedbackActivity.class);
                        startActivity(feedBackIntent);

                        break;
                }
                return true;
            }
        });

    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
        Intent homeIntent = new Intent(Intent.ACTION_MAIN);
        homeIntent.addCategory( Intent.CATEGORY_HOME );
        homeIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(homeIntent);
        finish();

    }
}
