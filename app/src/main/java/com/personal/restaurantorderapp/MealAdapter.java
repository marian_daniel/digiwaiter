package com.personal.restaurantorderapp;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

public class MealAdapter extends RecyclerView.Adapter<MealAdapter.mViewHolder> {
    private ArrayList<OrderItem> items;
    private View.OnClickListener mClickListener;
    public MealAdapter(ArrayList<OrderItem> items) {
        this.items=items;
    }
    public class mViewHolder extends  RecyclerView.ViewHolder {
        TextView name;
        TextView price;
        LinearLayout group;

        public mViewHolder(@NonNull View itemView) {
            super(itemView);
            this.name=itemView.findViewById(R.id.mealNameTV);
            this.price=itemView.findViewById(R.id.mealPrice);
            this.group=itemView.findViewById(R.id.mealGroup);
        }
    }
    @NonNull
    @Override
    public mViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.meal_item,viewGroup,false);
        mViewHolder holder=new mViewHolder(v);
        holder.group.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mClickListener.onClick(v);
            }
        });
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull mViewHolder myViewHolder, int i) {
        myViewHolder.name.setText(items.get(i).getName());
        myViewHolder.price.setText("Pret :"+items.get(i).getPrice()+" lei");
    }

    @Override
    public int getItemCount() {
        return items.size();
    }
    public void setClickListener(View.OnClickListener callback) {
        mClickListener = callback;
    }

}

