package com.personal.restaurantorderapp;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;


public class MainActivity extends AppCompatActivity {
    private RecyclerView orderListRV;
    private RecyclerView.LayoutManager layoutManager;
    private OrderListAdapter adapter;
    private ArrayList<OrderedItem> items;
    private TextView totalTV;
    private Button orderButton;
    private Button cleanOrder;
    private Intent addIntent;
    private Parcelable mParcelable;
    private LocalDatabase localDB;
    private Toolbar toolbar;
    private ImageView backIV;

    private int s;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        orderListRV=findViewById(R.id.orderListRecyclerView);
        orderButton=findViewById(R.id.orderButton);
        totalTV=findViewById(R.id.totalTV);
        toolbar=findViewById(R.id.checkoutToolbar);
        backIV=toolbar.findViewById(R.id.backIV);

        localDB=new LocalDatabase(this);
        cleanOrder=findViewById(R.id.cleanOrder);
        layoutManager=new LinearLayoutManager(this);
        items=new ArrayList<>();
        items=localDB.getCart();
        adapter=new OrderListAdapter(items,this,totalTV);
        orderListRV.setLayoutManager(layoutManager);
        adapter.notifyDataSetChanged();
        for(int i=0;i<items.size();i++)
            s = s + items.get(i).getPrice() * items.get(i).getQuantity();
        orderListRV.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        new ItemTouchHelper(itemTouchHelperCallBack).attachToRecyclerView(orderListRV);
        totalTV.setText("Total:" +s+" lei");

        cleanOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                localDB.clearCart();
                totalTV.setText("Total:"+0+" lei");
                adapter.clear();
                clearToast();
            }
        });

        backIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(getApplicationContext(),CategoryActivity.class);
                startActivity(intent);
            }
        });

        orderButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(items.isEmpty())
                    errorToast();
                else {
                    OrderDialog exampleDialog = new OrderDialog(getApplicationContext(),s);
                    exampleDialog.show(getSupportFragmentManager(), "example dialog");
                }
            }
        });
    }




    ItemTouchHelper.SimpleCallback itemTouchHelperCallBack = new ItemTouchHelper.SimpleCallback(0,ItemTouchHelper.RIGHT) {
        @Override
        public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder viewHolder1) {
            return false;
        }

        @Override
        public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
            s=s-items.get(viewHolder.getAdapterPosition()).getPrice()*items.get(viewHolder.getAdapterPosition()).getQuantity();
            totalTV.setText("Total:"+s+" lei");
           deleteToast();

            localDB.deleteCartItem(items.get(viewHolder.getAdapterPosition()));

            items.remove(viewHolder.getAdapterPosition());
            adapter.notifyDataSetChanged();

        }
    };

    private void prepareOrderData(){
        FirebaseFirestore firebaseFirestore=FirebaseFirestore.getInstance();
        ArrayList<String> categories= new ArrayList<String>();
        categories.add("Deserturi");
        categories.add("Felul 1");
        categories.add("Sosuri");
        categories.add("Fripturi");
        categories.add("Salate");
        for(int i=0;i<categories.size();i++)
        firebaseFirestore.collection(categories.get(i)).get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                for (DocumentSnapshot querySnapshot : task.getResult()) {
                    OrderedItem item = new OrderedItem(querySnapshot.getLong("id").intValue(),
                            querySnapshot.getString("name"),
                            querySnapshot.getLong("price").intValue(),
                            querySnapshot.getString("icon"),
                            querySnapshot.getLong("quantity").intValue(),
                            querySnapshot.getString("isFavorite"),
                            querySnapshot.getString("description"));

                    if (item.getQuantity()!=0) {
                        items.add(item);
                        orderListRV.setAdapter(adapter);
                        adapter.notifyDataSetChanged();
                        items.add(item);
                    }
                }
            }
        });
    }

    private void deleteToast() {
        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.adding_toast,null);
        TextView toastTV=layout.findViewById(R.id.toastText);
        ImageView toastIV=layout.findViewById(R.id.toastIcon);
        CardView toastCV=layout.findViewById(R.id.toastCV);
        toastTV.setText("Produsul a fost eliminat din lista !");
        Glide.with(getApplicationContext()).load(R.drawable.deleted_icon).into(toastIV);
        toastCV.setCardBackgroundColor(Color.parseColor("#FF4081"));
        Toast toast = new Toast(getApplicationContext());
        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 700);
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.setView(layout);
        toast.show();
    }

    private void clearToast() {

        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.adding_toast,null);
        TextView toastTV=layout.findViewById(R.id.toastText);
        ImageView toastIV=layout.findViewById(R.id.toastIcon);
        CardView toastCV=layout.findViewById(R.id.toastCV);
        toastTV.setText("Ati sters toate produsele din lista!");
        Glide.with(getApplicationContext()).load(R.drawable.deleted_icon).into(toastIV);
        toastCV.setCardBackgroundColor(Color.parseColor("#FF4081"));
        Toast toast = new Toast(getApplicationContext());
        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 700);
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.setView(layout);
        toast.show();
    }



    private void errorToast() {

        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.adding_toast,null);
        TextView toastTV=layout.findViewById(R.id.toastText);
        ImageView toastIV=layout.findViewById(R.id.toastIcon);
        CardView toastCV=layout.findViewById(R.id.toastCV);
        toastTV.setText("Comanda trebuie sa contina cel putin un produs!");
        Glide.with(getApplicationContext()).load(R.drawable.deleted_icon).into(toastIV);
        toastCV.setCardBackgroundColor(Color.parseColor("#FF4081"));
        Toast toast = new Toast(getApplicationContext());
        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 700);
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.setView(layout);
        toast.show();
    }
    public void onBackPressed()
    {
        super.onBackPressed();
        startActivity(new Intent(getApplicationContext(), CategoryActivity.class));
        finish();

    }



}
