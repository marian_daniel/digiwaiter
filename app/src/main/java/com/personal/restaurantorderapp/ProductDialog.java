package com.personal.restaurantorderapp;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatDialogFragment;

import com.bumptech.glide.Glide;

public class ProductDialog extends AppCompatDialogFragment {
    private ImageView iconIV;
    private TextView  descriptionTV;
    private String icon,description;
    private Context context;

    public ProductDialog(String icon, String description, Context context) {
        this.icon=icon;
        this.description=description;
        this.context=context;
    }

    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.product_dialog, null);

        builder.setView(view)
                .setPositiveButton("Inchide", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dismiss();
                    }
                });

        iconIV = view.findViewById(R.id.dialogIV);
        descriptionTV = view.findViewById(R.id.dialogTV);

        Glide.with(context).load(icon).into(iconIV);
        descriptionTV.setText(description);

        return builder.create();
    }
}
