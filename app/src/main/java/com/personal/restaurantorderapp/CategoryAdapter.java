package com.personal.restaurantorderapp;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.myViewHolder> {
    private ArrayList<Category> categories;
    private Context context;

    public CategoryAdapter(ArrayList<Category> categories,Context context) {
        this.categories=categories;
        this.context=context;
    }
    private View.OnClickListener mClickListener;
    public class myViewHolder extends RecyclerView.ViewHolder {
        TextView categoryName;
        ImageView categoryIcon;
        FrameLayout group;
        public myViewHolder(@NonNull View itemView) {
            super(itemView);
            this.categoryName=itemView.findViewById(R.id.categoryName);
            this.categoryIcon=itemView.findViewById(R.id.categoryIcon);
            this.group=itemView.findViewById(R.id.group);
        }
    }
    @Override
    public myViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.category_item,viewGroup,false);
        CategoryAdapter.myViewHolder mViewHolder=new CategoryAdapter.myViewHolder(v);
        mViewHolder.group.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                mClickListener.onClick(v);
            }
        });
        return mViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull myViewHolder myViewHolder, int i) {
        Category cat=categories.get(i);
        myViewHolder.categoryName.setText(categories.get(i).getName());
        Glide.with(context).load(cat.getIcon()).fitCenter()
                .into(myViewHolder.categoryIcon);
    }

    @Override
    public int getItemCount() {
        return categories.size();
    }

    public void setClickListener(View.OnClickListener callback) {
        mClickListener = callback;
    }
}