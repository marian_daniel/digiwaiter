package com.personal.restaurantorderapp;

public class OrderItem {
    private String icon;
    private String name;
    private int price;
    private int id;

    public OrderItem() {

    }
    public OrderItem(int id,String name,int price,String icon) {
        this.id=id;
        this.name=name;
        this.price=price;
        this.icon=icon;

    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }



    public String getIcon() {
        return this.icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

}

