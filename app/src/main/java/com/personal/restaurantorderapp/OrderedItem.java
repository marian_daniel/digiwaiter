package com.personal.restaurantorderapp;

public class OrderedItem extends OrderItem {
    private int quantity;
    private String isFavorite;
    private String description;
    public OrderedItem (){

    }
    public OrderedItem(int id, String name, int price, String icon,int quantity,String isFavorite,String description) {
        super(id, name, price, icon);
        this.isFavorite=isFavorite;
        this.quantity=quantity;
        this.description=description;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getIsFavorite() {
        return isFavorite;
    }

    public void setIsFavorite(String isFavorite) {
        this.isFavorite = isFavorite;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
