package com.personal.restaurantorderapp;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.firebase.firestore.FirebaseFirestore;

import org.w3c.dom.Text;

import java.util.ArrayList;

public class OrderListAdapter extends RecyclerView.Adapter<OrderListAdapter.MyViewHolder> {
    private ArrayList<OrderedItem> items;
    private Context context;
    private OrderedItem item;
    private LocalDatabase db;
    private TextView total;

    public OrderListAdapter(ArrayList <OrderedItem> items,Context context,TextView total) {
        this.items=items;
        this.context=context;
        this.total=total;
    }


    public static class MyViewHolder extends RecyclerView.ViewHolder {
        public ImageView itemIcon;
        public TextView itemName;
        public TextView itemPrice;
        public TextView itemQuantity;
        public ImageView add;
        public ImageView remove;
        public MyViewHolder(View itemView) {
            super(itemView);

            this.itemIcon=itemView.findViewById(R.id.orderItemIcon);
            this.itemName=itemView.findViewById(R.id.orderItemName);
            this.itemPrice=itemView.findViewById(R.id.orderItemPrice);
            this.itemQuantity=itemView.findViewById(R.id.orderItemQuantity);
            this.add=itemView.findViewById(R.id.addIV);
            this.remove=itemView.findViewById(R.id.removeIV);

        }
    }

    public OrderListAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View v= LayoutInflater.from(context).inflate(R.layout.orderlist_item,viewGroup,false);

        return new MyViewHolder(v) ;
    }

    @Override
    public void onBindViewHolder(@NonNull final OrderListAdapter.MyViewHolder myViewHolder, final int i) {
        item=items.get(i);
        final int position=i;
        Glide.with(context).load(item.getIcon()).into(myViewHolder.itemIcon);
        myViewHolder.itemName.setText(item.getName());
        myViewHolder.itemPrice.setText("Pret:"+item.getPrice()+ " lei");
        myViewHolder.itemQuantity.setText("Cantitate: "+item.getQuantity());
        db=new LocalDatabase(context);

        myViewHolder.add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                db.addQuantity(items.get(position).getId());
                items.get(position).setQuantity(items.get(position).getQuantity()+1);
                String suma=total.getText().toString().replaceAll("[^0-9]", "");
                int sum=Integer.parseInt(suma)+items.get(position).getPrice();
                total.setText("Total:"+sum+" lei");
                Log.d("Verificare",items.get(i).getQuantity()+"");
                notifyDataSetChanged();
            }
        });



             myViewHolder.remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (items.get(position).getQuantity()) {
                    case 0:
                        removeAt(position);
                        notifyDataSetChanged();
                        break;

                    case 1:
                         db.removeQuantity(items.get(position).getId());
                         items.get(position).setQuantity(items.get(position).getQuantity()-1);
                        String suma=total.getText().toString().replaceAll("[^0-9]", "");
                        int sum=Integer.parseInt(suma)-items.get(position).getPrice();
                        total.setText("Total:"+sum+" lei");
                         items.remove(position);
                         notifyDataSetChanged();
                         
                         break;
                    default:
                        db.removeQuantity(items.get(position).getId());
                        items.get(position).setQuantity(items.get(position).getQuantity()-1);
                        suma=total.getText().toString().replaceAll("[^0-9]", "");
                         sum=Integer.parseInt(suma)-items.get(position).getPrice();
                        total.setText("Total:"+sum+" lei");
                        notifyDataSetChanged();
                       break;
                }
            }
        });



    }

    @Override
    public int getItemCount() {
        return items.size();
    }
    public void clear() {
        int size = items.size();
        items.clear();
        notifyItemRangeRemoved(0, size);
    }

    public void removeAt(int position) {
        items.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, items.size());
    }

    public void addToCart(int i) {
        String category="Deserturi";
        switch(items.get(i).getId()/100) {
            case 1:
                category="Deserturi";
                break;
            case 2:
                category="Felul 1";
                break;
            case 3:
                category="Sosuri";
                break;
            case 4:
                category="Fripturi";
                break;
            case 5:
                category="Salata";

        }
        items.get(i).setQuantity(items.get(i).getQuantity()+1);
        FirebaseFirestore firestore=FirebaseFirestore.getInstance();
        firestore.collection(category).document(items.get(i).getName()).set(items.get(i));

    }

}

