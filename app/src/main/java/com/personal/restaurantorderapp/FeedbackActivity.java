package com.personal.restaurantorderapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.HashMap;
import java.util.Map;

public class FeedbackActivity extends AppCompatActivity {
private ImageView backIV;
private Toolbar toolbar;
private TextView toolbarTV;
private RatingBar rb;
private TextView feedbackTV;
private Button sendButton;
private String feedbackText;
private float feedbackRating;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback);

        toolbar=findViewById(R.id.backToolbar);
        backIV=toolbar.findViewById(R.id.backIV);
        toolbarTV=toolbar.findViewById(R.id.toolbarTV);

        toolbarTV.setText("Formular feedback");

        feedbackTV=findViewById(R.id.feedbackEditText);
        rb=findViewById(R.id.rating);
        sendButton=findViewById(R.id.sendFeedback);

        feedbackRating=rb.getRating();
        feedbackText=feedbackTV.getText().toString();


        backIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent backIntent=new Intent(getApplicationContext(),CategoryActivity.class);
                startActivity(backIntent);
            }
        });

        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                feedbackRating=rb.getRating();
                feedbackText=feedbackTV.getText().toString();

                Map<String,Object> feedback=new HashMap<>();
                feedback.put("feedback_text",feedbackText);
                feedback.put("feedback_rating",feedbackRating);
                FirebaseFirestore db=FirebaseFirestore.getInstance();

                db.collection("feedbacks").document().set(feedback).addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Toast.makeText(getApplicationContext(),"Feedback-ul a fost trimis ! Va multumim",Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
    }
}
