package com.personal.restaurantorderapp;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatDialogFragment;

public class OrderDialog extends AppCompatDialogFragment {
    private EditText table;
    private EditText preferences;
    private int s;
    private Context context;

    public OrderDialog(Context context ,int s) {
        this.context=context;
        this.s=s;
    }
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_layout, null);
        table = view.findViewById(R.id.table);
        preferences = view.findViewById(R.id.preferences);

        builder.setView(view)
                .setTitle("Informatii suplimentare")
                .setNegativeButton("Inapoi", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                })
                .setPositiveButton("Continuare", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                         String t= table.getText().toString();
                         String p= preferences.getText().toString();
                         Log.d("dada",s+"");
                         Intent intent=new Intent(context, OrderActivity.class);
                         intent.putExtra("table",t);
                         intent.putExtra("preferences",p);
                         intent.putExtra("total",s);
                        startActivity(intent);
                    }
                });



        return builder.create();
    }
}
