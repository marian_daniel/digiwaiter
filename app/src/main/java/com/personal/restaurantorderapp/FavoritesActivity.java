package com.personal.restaurantorderapp;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;

public class FavoritesActivity extends AppCompatActivity {
private GridView favoritesGV;
private RecyclerView.LayoutManager layoutManager;
private FavoritesAdapter adapter;
private ArrayList<OrderItem> favorites;
private LocalDatabase db;
private ArrayList<OrderedItem> items;
private Toolbar toolbar;
private ImageView backIV;
private TextView toolbarTV;

FirebaseFirestore  firebaseDB;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favorites);

        db=new LocalDatabase(this);
        favoritesGV=findViewById(R.id.favoritesGV);
        toolbar=findViewById(R.id.backToolbar);
        layoutManager=new LinearLayoutManager(this);
        backIV=toolbar.findViewById(R.id.backIV);
        toolbarTV=toolbar.findViewById(R.id.toolbarTV);

        toolbarTV.setText("Produse favorite");
        items=new ArrayList<OrderedItem>();


    prepareFavoritesData();

        adapter=new FavoritesAdapter(items,getApplicationContext(),getLayoutInflater(),getSupportFragmentManager());

        backIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(getApplicationContext(),CategoryActivity.class);
                startActivity(intent);
            }
        });

    }

    public void  prepareFavoritesData() {
        firebaseDB= FirebaseFirestore.getInstance();
        ArrayList<String> categories= new ArrayList<String>();
        categories.add("Deserturi");
        categories.add("Felul 1");
        categories.add("Sosuri");
        categories.add("Fripturi");
        categories.add("Salate");
        for(int i=0;i<categories.size();i++)
        firebaseDB.collection(categories.get(i)).get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                for (DocumentSnapshot querySnapshot : task.getResult()) {
                    OrderedItem item = new OrderedItem(querySnapshot.getLong("id").intValue(),
                            querySnapshot.getString("name"),
                            querySnapshot.getLong("price").intValue(),
                            querySnapshot.getString("icon"),
                            querySnapshot.getLong("quantity").intValue(),
                            querySnapshot.getString("isFavorite"),
                            querySnapshot.getString("description"));

                    if (item.getIsFavorite().equals("true")) {
                        items.add(item);
                        favoritesGV.setAdapter(adapter);
                        adapter.notifyDataSetChanged();
                    }
                }
            }
        });
    }
}

