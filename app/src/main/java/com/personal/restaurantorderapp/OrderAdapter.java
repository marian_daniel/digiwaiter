package com.personal.restaurantorderapp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class OrderAdapter extends RecyclerView.Adapter<OrderAdapter.MyViewHolder> {
private Context context;
private ArrayList<OrderedItem> items;
    public OrderAdapter(ArrayList<OrderedItem> items , Context context) {
this.items=items;
this.context=context;

    }
    public class MyViewHolder extends RecyclerView.ViewHolder {
   public TextView name;
   public TextView price;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            this.name=itemView.findViewById(R.id.productName);
            this.price=itemView.findViewById(R.id.productPrice);

        }
    }
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v= LayoutInflater.from(context).inflate(R.layout.orderdetail_item,parent,false);
        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
       holder.name.setText(items.get(position).getName());
       String price=items.get(position).getQuantity()+"buc. x "+items.get(position).getPrice()+ " lei = " + items.get(position).getQuantity()*items.get(position).getPrice() + " lei";
       holder.price.setText(price);
    }


    @Override
    public int getItemCount() {
        return items.size();
    }
}
