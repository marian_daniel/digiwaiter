package com.personal.restaurantorderapp;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;

public class MealListActivity extends AppCompatActivity {
    private Intent intent;
    private TextView title;
    private FirebaseFirestore firebaseDB;
    private ArrayList<OrderedItem> items;
    private LocalDatabase db;
    private String receivedName;
    private int id=0;
    private GridView gridView;
    private ProductsAdapter adapter;
    private Toolbar toolbar;
    private TextView toolbarTV;
    private ImageView backIV;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_meal_list);

        toolbar=findViewById(R.id.backToolbar);
        toolbarTV=toolbar.findViewById(R.id.toolbarTV);
        backIV=toolbar.findViewById(R.id.backIV);
        title=findViewById(R.id.title);
        items=new ArrayList<OrderedItem>();
        db=new LocalDatabase(this);
        gridView=findViewById(R.id.productsGV);
        intent=getIntent();
        receivedName=intent.getStringExtra("name");
        adapter=new ProductsAdapter(items,getApplicationContext(),getLayoutInflater(),receivedName,getSupportFragmentManager());
        gridView.setAdapter(adapter);


        toolbarTV.setText(receivedName);
        prepareMealsData();

        backIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(getApplicationContext(),CategoryActivity.class);
                startActivity(intent);
            }
        });

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("id",id);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        id=savedInstanceState.getInt("id");
    }

   public void  prepareMealsData() {
   firebaseDB=FirebaseFirestore.getInstance();
   firebaseDB.collection(receivedName).get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
       @Override
       public void onComplete(@NonNull Task<QuerySnapshot> task) {
           for(DocumentSnapshot querySnapshot: task.getResult()) {
               OrderedItem item =new OrderedItem(querySnapshot.getLong("id").intValue(),
                       querySnapshot.getString("name"),
                       querySnapshot.getLong("price").intValue(),
                       querySnapshot.getString("icon"),
                       querySnapshot.getLong("quantity").intValue(),
                       querySnapshot.getString("isFavorite"),
               querySnapshot.getString("description"));
               db.addProduct(item);
               items.add(item);
              gridView.setAdapter(adapter);
              adapter.notifyDataSetChanged();
           }


       }

   });
    }
    }

