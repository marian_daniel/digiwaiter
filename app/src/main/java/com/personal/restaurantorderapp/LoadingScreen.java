package com.personal.restaurantorderapp;

import android.content.Intent;
import android.graphics.Color;
import android.os.CountDownTimer;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ProgressBar;

public class LoadingScreen extends AppCompatActivity {
    private Intent intent;
    private ProgressBar pb;
    private final int time= 15*100;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_loading_screen);




        intent=new Intent(this,CategoryActivity.class);



        new CountDownTimer(time,50) {

            @Override
            public void onTick(long millisUntilFinished) {
                long finishedSeconds = time - millisUntilFinished;
                int total = (int) (((float)finishedSeconds / (float)time) * 100.0);


            }

            @Override
            public void onFinish() {
                startActivity(intent);
                finish();
            }
        }.start();
    }
}
